# SystemTech test task

[Task description](task-description.pdf)

## SQL Query used to select from database.db3

```sql
SELECT
	d.date          AS docDateTime,
	dt.name         AS docType,
	d.id            AS docId,
	p.id            AS productId,
	p.name          AS productName,
	p.image         AS productImage,
	p.price         AS productPrice,
	sum(r.quantity)	AS productQuantity,
	p.removed       AS productIsRemoved
FROM docs AS d
JOIN docTypes dt  ON d.typeId     = dt.id
JOIN rows r       ON r.docId      = d.id
JOIN products p   ON r.productId  = p.id
WHERE d.removed       = 0
      AND dt.removed  = 0
      AND r.removed   = 0
GROUP BY date(d.date), docId, productId
ORDER BY date(d.date), datetime(d.date);
```

## Installation

1. Clone this repo

```bash
git clone git@bitbucket.org:romengrus/systemtech-test.git
```

or

```bash
git clone https://romengrus@bitbucket.org/romengrus/systemtech-test.git
```

2. Inside cloned repo run

```bash
npm i && npm run start
```

3. Open browser at [localhost:1234](http://localhost:1234)
