module.exports = {
  env: {
    browser: true,
  },
  extends: ['airbnb-base', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error'],
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,
        optionalDependencies: false,
        peerDependencies: false,
      },
    ],
    'no-underscore-dangle': 'off',
    'class-methods-use-this': 'off',
  },
  overrides: [
    {
      files: ['**/*.test.js'],
      env: {
        mocha: true,
      },
      rules: {
        'func-names': 'off',
        'no-unused-expressions': 'off',
      },
    },
  ],
};
