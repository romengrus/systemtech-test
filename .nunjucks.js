const path = require('path');
const fs = require('fs');
const { formatDocs, formatDateDayMonth, formatPrice } = require('./src/scripts/formatters');

const docsFilePath = path.join(__dirname, 'src/data/docs.json');

function getDocs(filePath) {
  const file = fs.readFileSync(filePath);
  const docs = JSON.parse(file);

  return formatDocs(docs);
}

module.exports = {
  data: {
    docsByDayDict: getDocs(docsFilePath),
  },
  filters: {
    dateAsDayMonth: formatDateDayMonth,
    asPrice: formatPrice,
  },
};
