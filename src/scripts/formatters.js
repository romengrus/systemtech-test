const moment = require('moment');
const { groupBy, pipe, map, prop, applySpec, head, reduce, omit } = require('ramda');

function formatDateDayMonth(date, format = 'D MMMM', locale = 'ru') {
  moment.locale(locale);
  return moment(date).format(format);
}

function formatDateISO(date) {
  return moment(date).format('YYYY-MM-DD');
}

function formatPrice(price) {
  const formatter = new Intl.NumberFormat('ru-RU', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });

  if (typeof price === 'string') {
    return formatter.format(parseFloat(price));
  }

  return formatter.format(price);
}

/**
 * Format docs object
 *
 * @param {*} docs array of doc objects
 *            [
 *              {
 *                docDateTime: string,
 *                docType: oneOf("Приход" | "Расход")
 *                docId: number,
 *                productId: number,
 *                productName: string,
 *                productImage: string,
 *                productPrice: number,
 *                productQuantity: number,
 *                productIsRemoved: number
 *              },
 *              ...
 *            ]
 *
 * @result object mapping days to groups of documents, each containing product(s)
 *            {
 *              "2017-11-29": { <--------------------------------- day
 *                 total: number, <------------------------------- money of this day
 *                 docs: { <-------------------------------------- documents of this day
 *                    564564867361365: { <------------------------ document id
 *                      total: number, <-------------------------- money of this document
 *                      docType: oneOf("Приход" | "Расход")
 *                      docDateTime: string,
 *                      products: [ <----------------------------- products in this document
 *                        {
 *                          docId: number,
 *                          productId: number,
 *                          productName: string,
 *                          productImage: string,
 *                          productPrice: number,
 *                          productQuantity: number,
 *                          productIsRemoved: number
 *                        }
 *                      ]
 *                    },
 *                    ...
 *                ]
 *              },
 *              ...
 *            }
 */
function formatDocs(docs) {
  if (!docs) return [];

  const getTotal = reduce((acc, cur) => cur.productPrice * cur.productQuantity + acc, 0);

  // prettier-ignore
  const result = pipe(
    Array.from,
    // group documents by day
    groupBy(
      pipe(
        prop('docDateTime'), 
        formatDateISO
      )
    ),
    map(
      // each day must have fields: total, docs
      applySpec({
        // sum of money from all docs in this day
        total: getTotal,
        // object with docId as keys
        docs: pipe(
          groupBy( prop('docId') ),
          map(
            // each document mus have fields: total, products
            applySpec({
              // sum of money from all products in this document
              total: getTotal,
              docDateTime: pipe(head, prop('docDateTime')),
              docType: pipe(head, prop('docType')),
              // list of products
              products: map(omit(['docDateTime', 'docType']))
            }),
          )
        )
      }),
    )
  )(docs);

  return result;
}

module.exports = { formatDateDayMonth, formatDateISO, formatDocs, formatPrice };
