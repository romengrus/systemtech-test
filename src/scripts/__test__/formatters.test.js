const path = require('path');
const fs = require('fs');
const { expect } = require('chai');
const { formatDateDayMonth, formatDocs, formatPrice } = require('../formatters');

let mockedDocs = null;

beforeEach(function () {
  const docsFile = fs.readFileSync(path.join(__dirname, './mocks/docs.json'));
  // eslint-disable-next-line
  mockedDocs = JSON.parse(docsFile);
});

describe('formatDateDayMonth', function () {
  it('should return "day month" formatted date', function () {
    expect(formatDateDayMonth(new Date('2020-06-06'))).to.eq('6 июня');
    expect(formatDateDayMonth('2020-06-06')).to.eq('6 июня');
  });
});

describe('formatDocs', function () {
  it('should return empty array on empty input', function () {
    expect(formatDocs()).to.eql([]);
  });

  it('should group docs by day', function () {
    expect(Object.keys(formatDocs(mockedDocs)).length).to.eq(4);
  });
});

describe('formatPrice', function () {
  it('should return localized version of price', function () {
    expect(formatPrice(750)).to.eq('750.00');
    expect(formatPrice(524.567)).to.eq('524.57');
    expect(formatPrice(524453)).to.eq('524,453.00');
    expect(formatPrice(3751.32)).to.eq('3,751.32');
  });
});
