const path = require('path');
const fs = require('fs');
const { Database, verbose } = require('sqlite3');

/* eslint-disable no-alert, no-console */

const dbFilePath = path.join(__dirname, '../data/database.db3');
const dumpFilePath = path.join(__dirname, '../data/docs.json');
const selectDocsQ = `
	SELECT
		d.date			    AS docDateTime,
		dt.name         AS docType,
		d.id            AS docId,
		p.id            AS productId,
		p.name          AS productName,
		p.image         AS productImage,
		p.price         AS productPrice,
		sum(r.quantity)	AS productQuantity,
		p.removed       AS productIsRemoved
	FROM docs AS d
	JOIN docTypes dt  ON d.typeId     = dt.id
	JOIN rows r       ON r.docId      = d.id
	JOIN products p   ON r.productId  = p.id
	WHERE d.removed 	    = 0
				AND dt.removed  = 0
				AND r.removed   = 0
	GROUP BY date(d.date), docId, productId
	ORDER BY date(d.date), datetime(d.date);
`;

function log(err, successMsg, failMsg) {
  if (err instanceof Error) {
    console.log(failMsg);
  } else {
    console.log(successMsg);
  }
}

function dumpAsJSON(dumpTo) {
  return function dumpToFile(err, rows) {
    const asJson = JSON.stringify(rows, null, 4);
    fs.writeFileSync(dumpTo, asJson);

    log(err, `${rows.length} of selected docs dumped to ${dumpTo}`, `Something went wrong: ${err}`);
  };
}

if (!fs.existsSync(dbFilePath)) {
  throw new Error(`
    No database file found. 
    Please add "${path.basename(dbFilePath)}" to "${path.dirname(dbFilePath)}"
  `);
}

verbose();
const db = new Database(dbFilePath);

db.all(selectDocsQ, dumpAsJSON(dumpFilePath));

db.close();
