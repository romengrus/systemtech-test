import $ from 'jquery';

function factory(element, init, rootEl) {
  // if single element is provided
  if (element instanceof HTMLElement) {
    return init(element);
  }

  // if array of element provided
  if (Array.isArray(element)) {
    return $(element).map((idx, el) => init(el));
  }
  // if element is query selector and root is provided
  if (rootEl) {
    return $(rootEl)
      .find(element)
      .map((idx, el) => init(el));
  }

  return $(element).map((idx, el) => init(el));
}

export default factory;
