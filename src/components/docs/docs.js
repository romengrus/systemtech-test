import $ from 'jquery';

import factory from '../../scripts/factory';
import { doc, docQS } from '../doc/doc';

const docsQS = '.js-docs-by-day';
const dayQS = '.js-day';
const dayHeaderQS = '.js-day__header';
const dayDocsQS = '.js-day__docs';

function toggleDocsVisibility(rootEl) {
  $(rootEl)
    .find(dayHeaderQS)
    .on('click', function handleDayHeaderClick(e) {
      $(e.target).closest(dayQS).toggleClass('day_is-opened').find(dayDocsQS).toggle();
    });
}

function docs(el) {
  toggleDocsVisibility(el);

  // init child components
  factory(docQS, doc, el);
}

export { docs, docsQS };
