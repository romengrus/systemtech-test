import $ from 'jquery';

const docQS = '.js-doc';
const docHeaderQS = '.js-doc__header';
const docProductsHeaderQS = '.js-doc__products-header';
const docProductsQS = '.js-doc__products';

function toggleProductsVisibility(rootEl) {
  const docHeader = $(rootEl).find(docHeaderQS);
  const productHeader = $(rootEl).find(docProductsHeaderQS);

  const toggleProducts = (e) => {
    $(e.target).closest(docQS).toggleClass('doc_is-opened').find(docProductsQS).toggle();
  };

  docHeader.on('click', toggleProducts);
  productHeader.on('click', toggleProducts);
}

function doc(el) {
  toggleProductsVisibility(el);
}

export { doc, docQS };
